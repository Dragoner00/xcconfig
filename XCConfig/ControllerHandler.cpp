#include "ControllerHandler.h"
#include <future>
#include "MouseMoveHandler.h"
#include "ButtonHandler.h"
#include "MouseButtonHandler.h"
#include <Xinput.h>

unsigned int ControllerHandler::s_sleepTime = 5;

ControllerHandler::ControllerHandler(DWORD controllerID)
	: m_controllerID(controllerID)
{
	m_buttonMapping.insert({ XINPUT_GAMEPAD_A, std::make_shared<MouseButtonHandler>(MOUSEEVENTF_LEFTDOWN, MOUSEEVENTF_LEFTUP) });
	m_buttonMapping.insert({ XINPUT_GAMEPAD_B, std::make_shared<MouseButtonHandler>(MOUSEEVENTF_RIGHTDOWN, MOUSEEVENTF_RIGHTUP) });
	m_buttonMapping.insert({ XINPUT_GAMEPAD_DPAD_UP, std::make_shared<ButtonHandler>('W') });
	m_buttonMapping.insert({ XINPUT_GAMEPAD_DPAD_DOWN, std::make_shared<ButtonHandler>('S') });
	m_buttonMapping.insert({ XINPUT_GAMEPAD_DPAD_LEFT, std::make_shared<ButtonHandler>('A') });
	m_buttonMapping.insert({ XINPUT_GAMEPAD_DPAD_RIGHT, std::make_shared<ButtonHandler>('D') });
	m_buttonMapping.insert({ XINPUT_GAMEPAD_LEFT_SHOULDER, std::make_shared<ButtonHandler>(VK_TAB) });
	m_buttonMapping.insert({ XINPUT_GAMEPAD_RIGHT_SHOULDER, std::make_shared<ButtonHandler>('E') });
	m_buttonMapping.insert({ XINPUT_GAMEPAD_X, std::make_shared<ButtonHandler>('Q') });
	m_buttonMapping.insert({ XINPUT_GAMEPAD_Y, std::make_shared<ButtonHandler>('R') });
	m_buttonMapping.insert({ XINPUT_GAMEPAD_BACK, std::make_shared<ButtonHandler>(VK_ESCAPE) });

	m_stateHandlers.push_back(std::make_shared<MouseMoveHandler>(StickID::LEFTSTICK));
	m_stateHandlers.push_back(std::make_shared<MouseMoveHandler>(StickID::RIGHTSTICK));
}

void ControllerHandler::runMapping()
{
	auto mappingFuture = std::async(std::launch::async, [
		&m_stop = m_stop, controllerID = m_controllerID, sleepTime = s_sleepTime,
		&m_buttonMapping = m_buttonMapping, &m_stateHandlers = m_stateHandlers]()
		{

			// Memory to store the result of the function getting the state of
			// the controller
			DWORD resultGetState;

			// This loop gets the state of the controler and invokes mouse and/or
			// keyboard inputs
			while (!m_stop) 
			{
				// Memory to store the state of the controller
				XINPUT_STATE padState;
				ZeroMemory(&padState, sizeof(XINPUT_STATE));

				// Simply get the state of the controller from XInput.
				resultGetState = XInputGetState(controllerID, &padState);

				if (resultGetState == ERROR_SUCCESS)
				{
					// Iterate over all mappings
					for (auto& [mask, handler] : m_buttonMapping) {
						handler->handleState(padState.Gamepad.wButtons & mask);
					}

					for (auto& handler : m_stateHandlers) {
						handler->handleState(padState);
					}
					
				}
				else
				{
					//std::cout << ": is not connceted." << std::endl;
				}
				Sleep(sleepTime);
			};
		});

}
