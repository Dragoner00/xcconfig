#pragma once
#include "StickHandlerBase.h"
#include <Windows.h>
#include <WinNT.h>

class MouseMoveHandler : public StickHandlerBase
{
public:
	MouseMoveHandler(StickID stickID);	

	void handleState(const XINPUT_STATE& padState) override;

private:
	void moveRelativ(LONG dx, LONG dy);

	INPUT m_mouseInput;
	double m_mouseSpeed{ 20 };

};

