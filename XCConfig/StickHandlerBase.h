#pragma once
#include "IStateHandler.h"
#include "EnumStick.h"

class StickHandlerBase : public IStateHandler 
{
public:
	StickHandlerBase(StickID stickID);	

protected:
	StickID m_stickID;
};