// XCConfig.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include <iostream>
#include <Windows.h>
#include <Xinput.h>
#include <WinBase.h>
#include <WinError.h>
#include "PadInputHelper.h"
#include "MouseMoveHandler.h"
#include "ControllerHandler.h"

void pressKeyB(char mK)
{
	HKL kbl = GetKeyboardLayout(0);
	INPUT ip;
	ip.type = INPUT_KEYBOARD;
	ip.ki.time = 0;
	ip.ki.dwFlags = KEYEVENTF_UNICODE;
	if ((int)mK < 65 && (int)mK>90) //for lowercase
	{
		ip.ki.wScan = 0;
		ip.ki.wVk = VkKeyScanEx(mK, kbl);
	}
	else //for uppercase
	{
		ip.ki.wScan = mK;
		ip.ki.wVk = 0;

	}
	ip.ki.dwExtraInfo = 0;
	SendInput(1, &ip, sizeof(INPUT));
}
void pressKeyB2(char mK)
{
	HKL kbl = GetKeyboardLayout(0);
	INPUT ip;
	ip.type = INPUT_KEYBOARD;
	ip.ki.time = 0;
	ip.ki.dwFlags = 0; // Zero for key down
	//ip.ki.wScan = MapVirtualKey(0x58, 0);
	ip.ki.wVk = static_cast<int>(mK);
	ip.ki.dwExtraInfo = 0;
	SendInput(1, &ip, sizeof(INPUT));

	
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));
}
int main()
{
	ControllerHandler controllerZero(0);

	controllerZero.runMapping();
}