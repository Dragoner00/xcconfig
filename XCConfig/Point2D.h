#pragma once

template<typename T>
class Point2D {
public:
	Point2D();
	Point2D(const T& x, const T& y);

	void set(const T& x, const T& y);
	void setX(const T& x);
	void setY(const T& y);

	bool isZero();

	T getX() const;
	T getY() const;
private:
	T m_x;
	T m_y;
};

template<typename T>
Point2D<T>::Point2D()
	: m_x(0)
	, m_y(0)
{}

template<typename T>
Point2D<T>::Point2D(const T& x, const T& y)
	: m_x(x)
	, m_y(y)
{}

template<typename T>
void Point2D<T>::set(const T& x, const T& y) {
	m_x = x;
	m_y = y;
}

template<typename T>
void Point2D<T>::setX(const T& x) {
	m_x = x;
}

template<typename T>
void Point2D<T>::setY(const T& y) {
	m_y = y;
}

template<typename T>
bool Point2D<T>::isZero() {
	return (m_x == 0) && (m_y == 0);
}

template<typename T>
T Point2D<T>::getX() const {
	return m_x;
}

template<typename T>
T Point2D<T>::getY() const {
	return m_y;
}

