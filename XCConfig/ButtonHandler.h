#pragma once
#include <Windows.h>
#include "ButtonActionBase.h"

class ButtonHandler : public ButtonActionBase
{
public:
	ButtonHandler(unsigned char virtualButtonCode);

	void buttonPressed() override;

	void buttonReleased() override;

	static unsigned char getVirtualCode(char character);

private:
	unsigned char m_virtualButtonCode;
	INPUT m_keyboardInput;
	bool m_pressed;
};

