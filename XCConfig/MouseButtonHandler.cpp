#include "MouseButtonHandler.h"

MouseButtonHandler::MouseButtonHandler(DWORD flagDown, DWORD flagUp)
	: m_eventFlagDown(flagDown)
	, m_eventFlagUp(flagUp)
{
	m_mouseInput.type = INPUT_MOUSE;
	m_mouseInput.mi.dx = 0;
	m_mouseInput.mi.dy = 0;
	m_mouseInput.mi.mouseData = 0;
	m_mouseInput.mi.dwExtraInfo = 0;
	m_mouseInput.mi.time = 0;
	m_mouseInput.mi.dwFlags = 0;
}

void MouseButtonHandler::buttonPressed()
{
	if (!m_pressed) {
		m_mouseInput.mi.dwFlags = m_eventFlagDown;
		SendInput(1, &m_mouseInput, sizeof(INPUT));
		m_pressed = true;
	}
}

void MouseButtonHandler::buttonReleased()
{
	if (m_pressed) {
		m_mouseInput.mi.dwFlags = m_eventFlagUp;
		SendInput(1, &m_mouseInput, sizeof(INPUT));
		m_pressed = false;
	}
}

