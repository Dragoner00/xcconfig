#include "PadInputHelper.h"
#include <Windows.h>
#include <Xinput.h>

#pragma warning( disable : 26451)
Point2DFloat PadInputHelper::getZonedMovement(const StickID stickID, const XINPUT_STATE& state)
{
    Point2DFloat normalizedPos;

    unsigned int deadzone = 0;
    float stickX = 0;
    float stickY = 0;

    if (StickID::LEFTSTICK == stickID) {
        deadzone = XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE;
        stickX = state.Gamepad.sThumbLX;
        stickY = state.Gamepad.sThumbLY;
    }
    else {
        deadzone = XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE;
        stickX = state.Gamepad.sThumbRX;
        stickY = state.Gamepad.sThumbRY;
    }

    // determine how far the controller is pushed
    float magnitude = static_cast<float>(sqrt(stickX * stickX + stickY * stickY));

    // determine the direction the controller is pushed
    float normalizedStickX = stickX / magnitude;
    float normalizedStickY = stickY / magnitude;

    float normalizedMagnitude = 0;

    // check if the controller is outside a circular dead zone
    if (magnitude > deadzone)
    {
        // clip the magnitude at its expected maximum value
        if (magnitude > 32767) magnitude = 32767;

        // adjust magnitude relative to the end of the dead zone
        magnitude -= deadzone;

        // optionally normalize the magnitude with respect to its expected range
        // giving a magnitude value of 0.0 to 1.0
        normalizedMagnitude = magnitude / (32767 - deadzone);

        normalizedPos.set(stickX/32768, stickY/32768);
    }
    else // if the controller is in the deadzone zero out the magnitude
    {
        // magnitude = 0.0;
        // normalizedMagnitude = 0.0;

        normalizedPos.set(0, 0);
    }

    return normalizedPos;
}
