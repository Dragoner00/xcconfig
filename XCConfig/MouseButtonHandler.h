#pragma once
#include "ButtonActionBase.h"
#include <Windows.h>
class MouseButtonHandler : public ButtonActionBase
{
public:
	MouseButtonHandler(DWORD flagDown, DWORD flagUp);

	void buttonPressed() override;

	void buttonReleased() override;

private:
	DWORD m_eventFlagDown;
	DWORD m_eventFlagUp;
	INPUT m_mouseInput;
	bool m_pressed{ false };
};

