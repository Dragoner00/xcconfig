#include "MouseMoveHandler.h"
#include "PadInputHelper.h"

//#include <Windows.h>
//#include <WinUser.h>

MouseMoveHandler::MouseMoveHandler(StickID stickID)
	: StickHandlerBase(stickID)
{
	m_mouseInput.type = INPUT_MOUSE;
	m_mouseInput.mi.dwFlags = MOUSEEVENTF_MOVE;
}

void MouseMoveHandler::moveRelativ(LONG dx, LONG dy)
{
	m_mouseInput.mi.dx = dx;
	m_mouseInput.mi.dy = dy;
	SendInput(1, &m_mouseInput, sizeof(INPUT));
}

void MouseMoveHandler::handleState(const XINPUT_STATE& padState)
{
	// Get the normalized and deadzoned stick amplitude
	auto stickPosition = PadInputHelper::getZonedMovement(m_stickID, padState);

	// Move mouse
	if (!stickPosition.isZero()) {
		moveRelativ(static_cast<LONG>(m_mouseSpeed * stickPosition.getX()),
			-static_cast<LONG>(m_mouseSpeed * stickPosition.getY()));
	}
}
