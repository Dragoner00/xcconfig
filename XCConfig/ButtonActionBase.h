#pragma once

class ButtonActionBase {
public:
	virtual void buttonPressed() = 0;
	virtual void buttonReleased() = 0;
	virtual void handleState(bool pressed);
};