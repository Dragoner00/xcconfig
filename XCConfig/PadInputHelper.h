#pragma once
#include "Point2DFloat.h"
#include "EnumStick.h"

// Forward declaration
typedef struct _XINPUT_STATE XINPUT_STATE;

class PadInputHelper
{
public:
	PadInputHelper() = delete;

	static Point2DFloat getZonedMovement(const StickID stickID, const XINPUT_STATE& state);

};

