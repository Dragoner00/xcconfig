#pragma once
#include "Point2D.h"
#include <utility>

class Point2DFloat : public Point2D<float>
{
public:
	template<typename ... Args>
	Point2DFloat(Args&&... args) : Point2D(std::forward<Args>(args) ...) {};
};

