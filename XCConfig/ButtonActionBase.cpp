#include "ButtonActionBase.h"

void ButtonActionBase::handleState(bool pressed)
{
	if (pressed) {
		this->buttonPressed();
	}
	else {
		this->buttonReleased();
	}

}
