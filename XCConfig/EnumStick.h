#pragma once

enum class StickID {
	LEFTSTICK,
	RIGHTSTICK
};