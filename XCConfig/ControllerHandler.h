#pragma once
#include <Windows.h>
#include <vector>
#include <map>
#include <memory>

class ButtonActionBase;
class IStateHandler;

class ControllerHandler
{
public:
	ControllerHandler(DWORD controllerID);

	void runMapping();

private:

	/// <summary>
	/// Mapping between the bitmask for a controller button to a keyboard or mouse button.
	/// </summary>
	std::map<WORD, std::shared_ptr<ButtonActionBase>> m_buttonMapping;

	std::vector<std::shared_ptr<IStateHandler>> m_stateHandlers;

	/// <summary>
	/// The thread created by #runMapping() runs till this is set to true.
	/// </summary>
	bool m_stop{ false };

	/// <summary>
	/// XINPUT assigns each controller an ID in the range [0, 3]. 
	/// </summary>
	DWORD m_controllerID;

	/// <summary>
	/// The time in milli second the #runMapping() thread sleep after one iteration
	/// </summary>
	static unsigned int s_sleepTime;
};

