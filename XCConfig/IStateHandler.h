#pragma once

// Forward declaration
typedef struct _XINPUT_STATE XINPUT_STATE;

class IStateHandler {
public:
	virtual void handleState(const XINPUT_STATE& padState) = 0;
};