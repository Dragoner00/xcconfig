#include "ButtonHandler.h"
ButtonHandler::ButtonHandler(unsigned char virtualButtonCode)
	: m_virtualButtonCode(virtualButtonCode)
	, m_pressed(false)
{
	m_keyboardInput.type = INPUT_KEYBOARD;
	m_keyboardInput.ki.time = 0;
	m_keyboardInput.ki.dwFlags = 0; // Zero for key down
	m_keyboardInput.ki.wVk = m_virtualButtonCode;
	m_keyboardInput.ki.dwExtraInfo = 0;
}

void ButtonHandler::buttonPressed()
{
	if (!m_pressed) {
		m_keyboardInput.ki.dwFlags = 0; // Zero for key down
		SendInput(1, &m_keyboardInput, sizeof(INPUT));
		m_pressed = true;
	}
}

void ButtonHandler::buttonReleased()
{
	if (m_pressed) {
		m_keyboardInput.ki.dwFlags = KEYEVENTF_KEYUP;		
		SendInput(1, &m_keyboardInput, sizeof(INPUT));
		m_pressed = false;
	}
}

unsigned char ButtonHandler::getVirtualCode(char character)
{
	// Not impelemented
	return 0;
}
